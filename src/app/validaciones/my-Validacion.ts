import { AbstractControl } from '@angular/forms';

export class MyValidation {

    static validName(control: AbstractControl) {
        const l = control.value.toString().trim().length;
        if (l < 3 && l > 12) {
            return { invalidNombre2: false };
        }
        return null;
    }

    static validNameParametrizable(limit: number) {
        return (control: AbstractControl) => {
            const l = control.value.toString().trim().length;
            if (l < 3 || l > limit) {
                return { invalidNombre: true };
            }
            return null;
        }
    }
}