import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { db } from 'src/app/app.module';
import { ELEGIDO_FAVORITO, NUEVO_DESTINO } from 'src/app/redux/viajes.actions';
import { viajeState } from 'src/app/redux/viajes.reducers';
import { destinoViaje } from '../../models/destino-viaje.model';
import { DestinosApiClient } from '../../models/destinos-api-client.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<destinoViaje>;
  updates: string[];
  viajes$ = this.store.select('viajes');

  constructor(public destinosApiClient: DestinosApiClient, private store: Store<viajeState>) { 
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.destinosApiClient.subscribeOnChange((d: destinoViaje) => {
      if (d != null) {  
        this.updates.push('Se ha elegido a: ' +d.nombre);
      }
    });
  }

  ngOnInit(): void {
  }

  agregado(d: destinoViaje) {
    this.store.dispatch(NUEVO_DESTINO(d));
    if (d != null) {  
      this.updates.push('Se ha creado a: ' +d.nombre);
    }
    this.destinosApiClient.add(d); //Se agrega la tarjeta
    this.onItemAdded.emit(d);
    const myDb = db;
    myDb.destinos.add(d);
    console.log('Todos los destinos de la db!');
    myDb.destinos.toArray().then(destinos => console.log(destinos))
  }

  elegido(e: destinoViaje) {
    this.store.dispatch(ELEGIDO_FAVORITO(e));
    this.destinosApiClient.elegir(e);
  }


}
