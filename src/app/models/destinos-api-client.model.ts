import { fn } from '@angular/compiler/src/output/output_ast';
import { BehaviorSubject, PartialObserver, Subject } from 'rxjs';
import { destinoViaje } from './destino-viaje.model';

export class DestinosApiClient {
	destinos:destinoViaje[];
	current: Subject<any> = new BehaviorSubject<any>(null);
	constructor() {
       this.destinos = [];
	}

	add(d: destinoViaje){
	  this.destinos.push(d);
	  this.destinos.forEach(x => x.setSelected(false));
		d.setSelected(true);
		this.current.next(d);
	}

	getAll(){
	  return this.destinos;
    }

	elegir(d: destinoViaje) {
		this.destinos.forEach(x => x.setSelected(false));
		d.setSelected(true);
		this.current.next(d);
	}

	subscribeOnChange(fn: any) {
		this.current.subscribe(fn);
	}
} 