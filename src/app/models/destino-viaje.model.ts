export class destinoViaje {
    private selected!: boolean;
    public servicios: string[];

    constructor(public nombre: string, public u: string) {
        this.servicios = ['Desayuno', 'Almuerzo', 'Comida', 'Gym', 'Piscina', 'Spa'];
    }

    isSelected(): boolean {
        return this.selected;
    }
    setSelected(s: boolean) {
        this.selected = s;
    }
}