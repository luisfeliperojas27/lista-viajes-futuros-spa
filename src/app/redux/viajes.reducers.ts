import { createReducer, on } from "@ngrx/store";
import { destinoViaje } from "../models/destino-viaje.model";
import { NUEVO_DESTINO } from "./viajes.actions";



export interface viajeState {
    viajes: ReadonlyArray<destinoViaje>;
}

const initialState: ReadonlyArray<destinoViaje> = [];

export const viajeReducer = createReducer(
    initialState,
    on(NUEVO_DESTINO, (state, viaje) => [...state, viaje])
);

