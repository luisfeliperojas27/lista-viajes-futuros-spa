import { createAction, props } from "@ngrx/store";
import { destinoViaje } from "../models/destino-viaje.model";


export const NUEVO_DESTINO = createAction('[Destinos Viajes] Nuevo',
// props<{ viaje: destinoViaje }>()
(viaje: destinoViaje) => viaje
);

export const ELEGIDO_FAVORITO = createAction('[Destinos Viajes] Favorito',
(viaje: destinoViaje) => viaje);




