import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ReservasApiClientService {

  constructor() { }

  getAll() {
    return [{ id: 1, name: 'Número uno' }, {id: 2, name: 'Número dos' }];
  }
}
